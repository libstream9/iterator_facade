#ifndef STREAM9_ITERATOR_FACADE_HPP
#define STREAM9_ITERATOR_FACADE_HPP

#include "iterator_facade/concepts.hpp"
#include "iterator_facade/iterator_core_access.hpp"
#include "iterator_facade/type_traits.hpp"

#include <concepts>
#include <iterator>

namespace stream9 {

namespace rng = std::ranges;

template<typename Derived,
         typename Category,
         typename Value,
         typename Reference = Value&,
         typename Difference = std::ptrdiff_t >
    requires iter_detail::is_iterator_tag_v<Category>
          && iter_detail::is_value_v<Value>
          && std::signed_integral<Difference>
class iterator_facade
{
public:
    using iterator_category = Category;
    using value_type = Value;
    using reference = Reference;
    using difference_type = Difference;
    using pointer = std::conditional_t<std::is_reference_v<reference>,
                        std::add_pointer_t<std::remove_reference_t<reference>>,
                        void >;

private:
    template<typename Tag>
    static constexpr bool is_derived_from_category_v =
        std::derived_from<iterator_category, Tag>;

public:
    iterator_facade() noexcept
    {
        static_assert(is_valid_iterator_facade_base_v<Derived, Category>);
    }

    decltype(auto)
    operator*() const
        requires requires (Derived& i) {
                    iterator_core_access::dereference(i);
                 }
    {
        return iterator_core_access::dereference(derived());
    }

    decltype(auto)
    operator[](difference_type const n) const
        requires is_derived_from_category_v<std::random_access_iterator_tag>
    {
        return *(derived() + static_cast<difference_type>(n));
    }

    auto
    operator->() const
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && std::is_reference_v<reference>
    {
        return &operator*();
    }

    Derived&
    operator++()
        requires requires (Derived& i) {
                    iterator_core_access::increment(i);
                 }
    {
        iterator_core_access::increment(derived());

        return derived();
    }

    Derived
    operator++(int)
        requires requires (Derived& i) {
                    iterator_core_access::increment(i);
                 }
    {
        auto tmp = derived();
        ++*this;
        return tmp;
    }

    Derived&
    operator--()
        requires is_derived_from_category_v<std::bidirectional_iterator_tag>
              && requires (Derived& i) {
                  iterator_core_access::decrement(i);
              }
    {
        iterator_core_access::decrement(derived());

        return derived();
    }

    Derived
    operator--(int)
        requires is_derived_from_category_v<std::bidirectional_iterator_tag>
              && requires (Derived& i) {
                  iterator_core_access::decrement(i);
              }
    {
        auto tmp = derived();
        --*this;
        return tmp;
    }

    friend Derived
    operator+(Derived const& it, difference_type const n)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, n);
              }
    {
        auto tmp = it;
        tmp += n;
        return tmp;
    }

    friend Derived
    operator+(difference_type const n, Derived const& it)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, n);
              }
    {
        return operator+(it, n);
    }

    Derived&
    operator+=(difference_type const n)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, n);
              }
    {
        iterator_core_access::advance(derived(), n);

        return derived();
    }

    friend difference_type
    operator-(Derived const& lhs, Derived const& rhs)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived const& i, Derived const& j) {
                  iterator_core_access::distance_to(i, j);
              }
    {
        return iterator_core_access::distance_to(rhs, lhs);
    }

    friend Derived
    operator-(Derived const& it, difference_type const n)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, -n);
              }
    {
        auto tmp = it;
        tmp -= n;
        return tmp;
    }

    Derived&
    operator-=(difference_type const n)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, -n);
              }
    {
        iterator_core_access::advance(derived(), -n);

        return derived();
    }

    template<typename T>
        requires is_derived_from_category_v<std::input_iterator_tag>
              && requires (Derived const& lhs, T const& rhs) {
                  iterator_core_access::equal(lhs, rhs);
              }
    friend bool
    operator==(Derived const& lhs, T const& rhs)
    {
        return iterator_core_access::equal(lhs, rhs);
    }

    template<typename T>
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived const& lhs, T const& rhs) {
                  iterator_core_access::compare(lhs, rhs);
              }
    friend std::strong_ordering
    operator<=>(Derived const& lhs, T const& rhs)
    {
        return iterator_core_access::compare(lhs, rhs);
    }

private:
    Derived&
    derived()
    {
        return static_cast<Derived&>(*this);
    }

    Derived const&
    derived() const
    {
        return static_cast<Derived const&>(*this);
    }
};

} // namespace stream9

#include "iterator_facade/gcc_bug_96416_workaround.hpp"

#endif // STREAM9_ITERATOR_FACADE_HPP
