#ifndef STREAM9_ITERATOR_FACADE_GCC_BUG_96416_WORKAROUND_HPP
#define STREAM9_ITERATOR_FACADE_GCC_BUG_96416_WORKAROUND_HPP

namespace stream9::detail {

template<template<class...> class T, class U>
struct is_derived_from_template
{
private:
    template<typename... V>
    static decltype(static_cast<T<V...> const&>(std::declval<U>()), std::true_type{})
    test(T<V...> const&);

    static std::false_type test(...);

public:
    static constexpr bool value =
        decltype(is_derived_from_template::test(std::declval<U>()))::value;
};

} // namespace stream9::detail

namespace std {

template<typename T>
    requires stream9::detail::is_derived_from_template<stream9::iterator_facade, T>::value
struct pointer_traits<T>
{
    static auto
    to_address(T i)
    {
        return i.operator->();
    }
};

} // namespace std

#endif // STREAM9_ITERATOR_FACADE_GCC_BUG_96416_WORKAROUND_HPP
