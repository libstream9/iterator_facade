#ifndef STREAM9_ITERATOR_FACADE_TYPE_TRAITS_HPP
#define STREAM9_ITERATOR_FACADE_TYPE_TRAITS_HPP

#include <iterator>
#include <type_traits>

namespace stream9 {

    namespace iter_detail {

    template<typename T>
    inline constexpr bool is_iterator_tag_v =
           std::derived_from<T, std::input_iterator_tag>
        || std::derived_from<T, std::output_iterator_tag>;

    template<typename T>
    inline constexpr bool is_value_v = !std::is_reference_v<T>;

    } // namespace iter_detail

} // namespace stream9

#endif // STREAM9_ITERATOR_FACADE_TYPE_TRAITS_HPP
