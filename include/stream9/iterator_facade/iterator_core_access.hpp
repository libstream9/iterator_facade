#ifndef STREAM9_ITERATOR_FACADE_ITERATOR_CORE_ACCESS_HPP
#define STREAM9_ITERATOR_FACADE_ITERATOR_CORE_ACCESS_HPP

#include <concepts>
#include <compare>

namespace stream9 {

class iterator_core_access
{
public:
    template<typename Derived>
        requires requires (Derived const& d) {
            { d.dereference() } -> std::same_as<typename Derived::reference>;
        }
    static decltype(auto)
    dereference(Derived const& d)
    {
        return d.dereference();
    }

    template<typename Derived>
        requires requires (Derived& d) {
            d.increment();
        }
    static void
    increment(Derived& d)
    {
        d.increment();
    }

    template<typename Derived>
        requires requires (Derived& d) {
            d.decrement();
        }
    static void
    decrement(Derived& d)
    {
        d.decrement();
    }

    template<typename Derived, typename N>
        requires requires (Derived& d, N n) {
            d.advance(n);
        }
    static void
    advance(Derived& d, N const n)
    {
        d.advance(n);
    }

    template<typename Derived, typename Other>
        requires requires (Derived const& d, Other const& o) {
            { d.equal(o) } -> std::same_as<bool>;
        }
    static bool
    equal(Derived const& d, Other const& o)
    {
        return d.equal(o);
    }

    template<typename Derived, typename Other>
        requires requires (Derived const& d, Other const& o) {
            { d.compare(o) } -> std::same_as<std::strong_ordering>;
        }
    static std::strong_ordering
    compare(Derived const& d, Other const& o)
    {
        return d.compare(o);
    }

    template<typename Derived, typename Other>
        requires requires (Derived const& d, Other const& o) {
            { d.distance_to(o) }
                    -> std::same_as<typename Derived::difference_type>;
        }
    static auto
    distance_to(Derived const& d, Other const& o)
    {
        return d.distance_to(o);
    }
};

} // namespace stream9

#endif // STREAM9_ITERATOR_FACADE_ITERATOR_CORE_ACCESS_HPP
