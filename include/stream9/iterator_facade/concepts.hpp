#ifndef STREAM9_ITERATOR_FACADE_CONCEPTS_HPP
#define STREAM9_ITERATOR_FACADE_CONCEPTS_HPP

#include "iterator_core_access.hpp"

#include <iterator>
#include <compare>

namespace stream9 {

template<typename T>
concept input_iterator_base =
    requires (T& t) {
        { iterator_core_access::dereference(t) }
                                    -> std::same_as<typename T::reference>;
        iterator_core_access::increment(t);
    };

template<typename T>
concept forward_iterator_base =
    input_iterator_base<T> &&
    requires (T const& t) {
        { iterator_core_access::equal(t, t) } -> std::same_as<bool>;
    };

template<typename T>
concept bidirectional_iterator_base =
    forward_iterator_base<T> &&
    requires (T& t) {
        iterator_core_access::decrement(t);
    };

template<typename T>
concept random_access_iterator_base =
    bidirectional_iterator_base<T> &&
    requires (T&& i, T const& j, T::difference_type n) {
        iterator_core_access::advance(i, n);

        { iterator_core_access::distance_to(i, j) }
                    -> std::same_as<typename T::difference_type>;

        { iterator_core_access::compare(i, j) }
                    -> std::same_as<std::strong_ordering>;
    };

template<typename T>
concept contiguous_iterator_base =
    random_access_iterator_base<T>;

template<typename T, typename Category>
struct is_valid_iterator_facade_base : std::false_type {};

template<typename T, typename Category>
constexpr bool is_valid_iterator_facade_base_v =
    is_valid_iterator_facade_base<T, Category>::value;

template<input_iterator_base T>
struct is_valid_iterator_facade_base<T, std::input_iterator_tag>
    : std::true_type {};

template<forward_iterator_base T>
struct is_valid_iterator_facade_base<T, std::forward_iterator_tag>
    : std::true_type {};

template<bidirectional_iterator_base T>
struct is_valid_iterator_facade_base<T, std::bidirectional_iterator_tag>
    : std::true_type {};

template<random_access_iterator_base T>
struct is_valid_iterator_facade_base<T, std::random_access_iterator_tag>
    : std::true_type {};

template<contiguous_iterator_base T>
struct is_valid_iterator_facade_base<T, std::contiguous_iterator_tag>
    : std::true_type {};

} // namespace stream9

#endif // STREAM9_ITERATOR_FACADE_CONCEPTS_HPP
